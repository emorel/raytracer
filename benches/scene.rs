mod support;

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use raytracer::scene::World;

pub fn criterion_benchmark(c: &mut Criterion) {
    let empty_scene = black_box(World::new(1920, 1080));

    bench_fn!("scene_cast_prime", c, empty_scene.cast_prime(1.0, 1.0));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
