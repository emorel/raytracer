mod support;

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use raytracer::math::{self, Intersectable};
use raytracer::shapes;

pub fn criterion_benchmark(c: &mut Criterion) {
    let sphere = black_box(shapes::Sphere {
        center: math::Vec3::new(0.0, 0.0, -5.0),
        radius: 1.0,
        color: shapes::colors::WHITE,
    });
    let ray = black_box(math::Ray {
        origin: math::Vec3::zero(),
        direction: math::Vec3::x_axis(),
    });

    bench_fn!("sphere_intersect", c, sphere.intersect(&ray, 0.0, f64::MAX));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
