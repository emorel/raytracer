mod support;

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use raytracer::math;

pub fn criterion_benchmark(c: &mut Criterion) {
    let vec = black_box(math::Vec3::new(2.0, 3.0, 4.2));
    let other = black_box(math::Vec3::new(0.4, 1.5, 5.2));

    bench_fn!("vec3_ops_eq", c, vec == other);
    bench_fn!("vec3_ops_ne", c, vec != other);

    bench_fn!("vec3_ops_add_vec3", c, vec + other);
    bench_fn!("vec3_ops_sub_vec3", c, vec - other);
    bench_fn!("vec3_ops_mul_vec3", c, vec * other);
    bench_fn!("vec3_ops_mul_f64", c, vec * 2.0);
    bench_fn!("f64_ops_mul_vec3", c, 2.0 * vec);
    bench_fn!("vec3_ops_div_vec3", c, vec / other);
    bench_fn!("vec3_ops_div_f64", c, vec / 2.0);
    bench_fn!("f64_ops_div_vec3", c, 2.0 / vec);

    bench_fn!("vec3_functions_length", c, vec.length());
    bench_fn!("vec3_functions_length_recip", c, vec.length_recip());
    bench_fn!("vec3_functions_dot", c, vec.dot(other));
    bench_fn!("vec3_functions_cross", c, vec.cross(other));
    bench_fn!("vec3_functions_normalize", c, vec.normalize());

    black_box(vec);
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
