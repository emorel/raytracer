#[macro_export]
macro_rules! bench_fn {
    ($name:expr, $c:expr, $action:expr) => {
        $c.bench_function($name, |b| b.iter(|| $action));
    };
}
