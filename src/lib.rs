//! A 3D renderer based on ray-tracing technology.
//!
//! Absolutely not usable in a real-world scenario, this lib is a simple way to learn Rust.

#![deny(unsafe_code, unused_qualifications, unused_import_braces)]

#[macro_export]
macro_rules! assert_relative_eq {
    ($f1:expr, $f2:expr) => {
        if ($f1 - $f2).abs() < f64::EPSILON {
            assert!(true)
        } else {
            panic!("{} != {}", $f1, $f2)
        }
    };
}

#[macro_use]
pub mod math;
pub mod scene;
pub mod shapes;
