use std::process::exit;

use raytracer::{math, scene, shapes};

fn main() {
    let width = 400;
    let aspect_ratio = 16.0 / 9.0;
    let scene = {
        let mut world = scene::World::new(width, (width as f64 / aspect_ratio) as u32);

        world.background_color = shapes::Color::new(0.5, 0.7, 1.0);

        world.add_object(shapes::Sphere {
            center: math::Point::new(0.0, 0.0, -1.0),
            radius: 0.5,
            color: shapes::Color::new(0.5, 0.7, 1.0),
        });

        world
    };

    println!("=> Rendering scene...");
    let instant = std::time::Instant::now();
    let image = scene.render();
    println!("  => Rendered in : {:?}", instant.elapsed());

    // return;

    println!("=> Saving image...");
    match image.save("result.png") {
        Ok(_) => {}
        Err(e) => {
            println!("{}", e);
            exit(1)
        }
    }

    println!("=> Opening image...");
    match std::process::Command::new("gwenview")
        .arg("result.png")
        .output()
    {
        Ok(_) => exit(0),
        Err(e) => {
            println!("{}", e);
            exit(e.raw_os_error().unwrap_or(1))
        }
    }
}
