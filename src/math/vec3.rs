use crate::math::{X_AXIS, Y_AXIS, ZERO, Z_AXIS};
use std::cmp::PartialEq;
use std::ops::{Add, Div, Mul, Neg, Sub};

/// A structure representing a [vector][vector-wikipedia] in [Euclidean space][euclidean-wikipedia].
///
/// [euclidean-wikipedia]: https://en.wikipedia.org/wiki/Euclidean_space
/// [vector-wikipedia]: https://en.wikipedia.org/wiki/vectors
#[derive(Debug, Clone, Copy)]
pub struct Vec3 {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Vec3 {
    /// Creates a new `Vec3`.
    #[inline]
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        Vec3 { x, y, z }
    }

    /// Returns a new `Vec3` with all planes as 0.0.
    #[inline]
    pub fn zero() -> Self {
        ZERO
    }

    /// Returns a `Vec3` with x as 1.0.
    #[inline]
    pub fn x_axis() -> Self {
        X_AXIS
    }

    /// Returns a `Vec3` with y as 1.0.
    #[inline]
    pub fn y_axis() -> Self {
        Y_AXIS
    }

    /// Returns a `Vec3` with z as 1.0.
    #[inline]
    pub fn z_axis() -> Self {
        Z_AXIS
    }

    /// Calculates the magnitude (a.k.a. norm) of the vector.
    ///
    /// [Read more][wikipedia] about the length of vectors.
    ///
    /// [wikipedia]: https://en.wikipedia.org/wiki/Norm_(mathematics)
    #[inline]
    pub fn length(self) -> f64 {
        self.dot(self).sqrt()
    }

    /// Calculates the reciprocal length of the vector.
    #[inline]
    pub fn length_recip(self) -> f64 {
        self.length().recip()
    }

    /// Turns the vector into a [unit vector][wikipedia].
    ///
    /// # TODO: Add test for `unit()`.
    ///
    /// [wikipedia]: https://en.wikipedia.org/wiki/Unit_vector
    #[inline]
    pub fn unit(self) -> Vec3 {
        self / self.length()
    }

    /// Calculates the dot product between `self` and `other`.
    ///
    /// [Read more][wikipedia] about the dot product.
    ///
    /// [wikipedia]: https://en.wikipedia.org/wiki/Dot_product
    #[inline]
    pub fn dot(self, other: Vec3) -> f64 {
        self.x * other.x + self.y * other.y + self.z * other.z
    }

    /// Calculates the cross product between two `Vec3`.
    ///
    /// [Read more][wikipedia] about the cross product.
    ///
    /// [wikipedia]: https://en.wikipedia.org/wiki/Cross_product
    #[inline]
    pub fn cross(self, other: Vec3) -> Self {
        Self::new(
            self.y * other.z - self.z * other.y,
            self.z * other.x - self.x * other.z,
            self.x * other.y - self.y * other.x,
        )
    }

    /// Normalizes the vector.
    ///
    /// [Read more][wikipedia] about normalization.
    ///
    /// [wikipedia]: https://en.wikipedia.org/wiki/Unit_vector
    #[inline]
    pub fn normalize(self) -> Self {
        self * self.length_recip()
    }
}

impl PartialEq for Vec3 {
    /// Compares 2 `Vec3` together.
    ///
    /// Equality is defined as each member of `self` being equal to their respective member on `rhs`.
    #[inline]
    fn eq(&self, rhs: &Self) -> bool {
        (rhs.x == self.x) && (rhs.y == self.y) && (rhs.z == self.z)
    }
}

impl Add for Vec3 {
    type Output = Self;

    /// Adds 2 vectors together.
    #[inline]
    fn add(self, rhs: Self) -> Self::Output {
        Self::new(self.x + rhs.x, self.y + rhs.y, self.z + rhs.z)
    }
}

impl Sub for Vec3 {
    type Output = Self;

    /// Subtracts 2 vectors together.
    #[inline]
    fn sub(self, rhs: Self) -> Self::Output {
        Self::new(self.x - rhs.x, self.y - rhs.y, self.z - rhs.z)
    }
}

impl Mul<Vec3> for Vec3 {
    type Output = Self;

    /// Multiplies `Vec3` with another `Vec3`, returning a new `Vec3`
    #[inline]
    fn mul(self, rhs: Self) -> Self {
        Self::new(rhs.x * self.x, rhs.y * self.y, rhs.z * self.z)
    }
}

impl Mul<f64> for Vec3 {
    type Output = Self;

    /// Multiplies a `Vec3` with `f64`.
    #[inline]
    fn mul(self, rhs: f64) -> Self::Output {
        Self::new(rhs * self.x, rhs * self.y, rhs * self.z)
    }
}

impl Mul<Vec3> for f64 {
    type Output = Vec3;

    /// Multiplies a `Vec3` with `f64`.
    #[inline]
    fn mul(self, rhs: Vec3) -> Self::Output {
        rhs * self
    }
}

impl Div<Vec3> for Vec3 {
    type Output = Self;

    /// Divides a `Vec3` with `Vec3`.
    #[inline]
    fn div(self, rhs: Vec3) -> Self::Output {
        Self::new(self.x / rhs.x, self.y / rhs.y, self.z / rhs.z)
    }
}

impl Div<f64> for Vec3 {
    type Output = Vec3;

    /// Divides a `Vec3` with `f64`.
    #[inline]
    fn div(self, rhs: f64) -> Self::Output {
        Self::new(self.x / rhs, self.y / rhs, self.z / rhs)
    }
}

impl Div<Vec3> for f64 {
    type Output = Vec3;

    /// Divides a `f64` with `Vec3`.
    #[inline]
    fn div(self, rhs: Vec3) -> Self::Output {
        Vec3::new(self / rhs.x, self / rhs.y, self / rhs.z)
    }
}

impl Neg for Vec3 {
    type Output = Vec3;

    /// Negates a vector.
    #[inline]
    fn neg(self) -> Self::Output {
        Self::Output::new(-self.x, -self.y, -self.z)
    }
}

impl From<(f64, f64, f64)> for Vec3 {
    /// Converts a tuple of `f64` into a `Vec3`.
    #[inline]
    fn from(tuple: (f64, f64, f64)) -> Self {
        Self::new(tuple.0, tuple.1, tuple.2)
    }
}

impl From<Vec3> for (f64, f64, f64) {
    /// Converts a tuple of `f64` into a `Vec3`.
    #[inline]
    fn from(vec: Vec3) -> Self {
        (vec.x, vec.y, vec.z)
    }
}

impl From<[f64; 3]> for Vec3 {
    /// Converts a tuple of `f64` into a `Vec3`.
    #[inline]
    fn from(array: [f64; 3]) -> Self {
        Self::new(array[0], array[1], array[2])
    }
}

impl From<Vec3> for [f64; 3] {
    /// Converts a tuple of `f64` into a `Vec3`.
    #[inline]
    fn from(vec: Vec3) -> Self {
        [vec.x, vec.y, vec.z]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_vec3_constructors() {
        let vec = Vec3::new(-1.2, 4.3, 0.0);
        assert_relative_eq!(vec.x, -1.2);
        assert_relative_eq!(vec.y, 4.3);
        assert_relative_eq!(vec.z, 0.0);

        let vec: Vec3 = Vec3::zero();
        assert_relative_eq!(vec.x, 0.0);
        assert_relative_eq!(vec.y, 0.0);
        assert_relative_eq!(vec.z, 0.0);

        let vec: Vec3 = Vec3::x_axis();
        assert_relative_eq!(vec.x, 1.0);
        assert_relative_eq!(vec.y, 0.0);
        assert_relative_eq!(vec.z, 0.0);

        let vec: Vec3 = Vec3::y_axis();
        assert_relative_eq!(vec.x, 0.0);
        assert_relative_eq!(vec.y, 1.0);
        assert_relative_eq!(vec.z, 0.0);

        let vec: Vec3 = Vec3::z_axis();

        assert_relative_eq!(vec.x, 0.0);
        assert_relative_eq!(vec.y, 0.0);
        assert_relative_eq!(vec.z, 1.0);
    }

    #[test]
    fn test_vec3_ops() {
        let vec = Vec3::new(1.0, 2.0, 3.0);
        let vec2 = Vec3::new(1.0, 2.0, 3.0);

        assert_eq!(vec, vec2);
        assert_ne!(vec, -vec2);

        assert_eq!((2.0, 4.0, 6.0), (vec + vec2).into());
        assert_eq!((0.0, 0.0, 0.0), (vec - vec2).into());

        assert_eq!((2.0, 4.0, 6.0), (vec * 2.0).into());
        assert_eq!((2.0, 4.0, 6.0), (2.0 * vec2).into());
        assert_eq!((1.0, 4.0, 9.0), (vec * vec2).into());

        assert_eq!((1.0, 1.0, 1.0), (vec / vec2).into());
        assert_eq!((0.5, 1.0, 1.5), (vec / 2.0).into());
        assert_eq!((6.0, 3.0, 2.0), (6.0 / vec2).into());
    }

    #[test]
    #[allow(clippy::float_cmp)]
    fn test_vec3_casts() {
        let tuple: (f64, f64, f64) = Vec3::zero().into();
        assert_eq!((0.0, 0.0, 0.0), tuple);

        let vec: Vec3 = (0.0f64, 0.0f64, 0.0f64).into();
        assert_eq!(Vec3::zero(), vec);

        let array: [f64; 3] = Vec3::zero().into();
        assert_eq!([0.0, 0.0, 0.0], array);

        let vec: Vec3 = [0.0f64, 0.0f64, 0.0f64].into();
        assert_eq!(Vec3::zero(), vec);
    }

    #[test]
    fn test_vec3_functions() {
        let x = Vec3::x_axis();
        let y = Vec3::y_axis();
        let z = Vec3::z_axis();

        assert_relative_eq!(4.0, (-4.0 * x).length());
        assert_relative_eq!(2.0, (-2.0 * y).length());
        assert_relative_eq!(3.0, (-3.0 * z).length());

        assert_relative_eq!(
            1.0 / (2.0f64 * 2.0 + 3.0 * 3.0 + 4.0 * 4.0).sqrt(),
            Vec3::new(2.0, 3.0, 4.0).length_recip()
        );

        assert_relative_eq!(1.0, x.dot(x));
        assert_relative_eq!(0.0, x.dot(y));
        assert_relative_eq!(-1.0, z.dot(-z));

        assert_eq!(x, y.cross(z));
        assert_eq!(y, z.cross(x));
        assert_eq!(z, x.cross(y));

        assert_eq!(-x, z.cross(y));
        assert_eq!(-y, x.cross(z));
        assert_eq!(-z, y.cross(x));

        assert_eq!(x, (2.0 * x).normalize());
    }
}
