use crate::math::{Point, Vec3};
use std::fmt;

/// Represent any kind of object that has been intersected by a [Ray].
#[derive(Debug)]
pub struct Intersection {
    pub point: Point,
    pub normal: Vec3,
    pub t: f64,
    pub front_face: bool,
}

impl Intersection {
    pub fn face_normal(ray: &Ray, outward_normal: Vec3) -> (Vec3, bool) {
        let front_face = ray.direction.dot(outward_normal) < 0.;
        let normal = if front_face {
            outward_normal
        } else {
            -outward_normal
        };

        (normal, front_face)
    }
}

/// Any component that may be intersected by a [Ray].
pub trait Intersectable: fmt::Debug {
    fn intersect(&self, ray: &Ray, t_min: f64, t_max: f64) -> Option<Intersection>;
}

/// A line pointing towards a direction from a point.
#[derive(Debug, Clone, Copy)]
pub struct Ray {
    pub origin: Point,
    pub direction: Vec3,
}

impl Ray {
    /// Returns a point on the ray at `P(t) = A + tB`
    /// where `A` is the origin and `B` is the direction.
    #[inline]
    pub fn at(&self, t: f64) -> Point {
        self.origin + t * self.direction
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::math::{Point, Vec3, Z_AXIS};

    #[test]
    fn test_returns_point_at_t() {
        let ray = Ray {
            origin: Point::new(0., 0., 0.),
            direction: Vec3::new(1., 1., 1.),
        };

        let at = ray.at(2.);

        assert_eq!(at, Point::new(2., 2., 2.))
    }

    #[test]
    fn test_returns_face_normal() {
        let ray = Ray {
            origin: Point::new(0., 0., 0.),
            direction: Vec3::new(1., 1., 1.),
        };

        let (normal, front_face) = Intersection::face_normal(&ray, Z_AXIS);

        assert_eq!(normal, -Z_AXIS);
        assert!(!front_face);

        let ray = Ray {
            origin: Point::new(0., 0., 0.),
            direction: Vec3::new(-1., -1., -1.),
        };

        let (normal, front_face) = Intersection::face_normal(&ray, Z_AXIS);

        assert_eq!(normal, Z_AXIS);
        assert!(front_face);
    }
}
