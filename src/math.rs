mod ray;
mod vec3;

pub use ray::*;
pub use vec3::*;

/// A macro that creates a new [`Vec3`].
#[macro_export]
macro_rules! vec3 {
    ($x:expr, $y:expr, $z:expr) => {{
        $crate::math::Vec3 {
            x: $x,
            y: $y,
            z: $z,
        }
    }};
    ($xyz:expr) => {{
        $crate::math::Vec3 {
            x: $xyz,
            y: $xyz,
            z: $xyz,
        }
    }};
}

/// A default `Vec3` with all axis at 0.
pub const ZERO: Vec3 = vec3![0.0];
/// A default `Vec3` with the X axis at 1.
pub const X_AXIS: Vec3 = vec3![1.0, 0.0, 0.0];
/// A default `Vec3` with the Y axis at 1.
pub const Y_AXIS: Vec3 = vec3![0.0, 1.0, 0.0];
/// A default `Vec3` with the Z axis at 1.
pub const Z_AXIS: Vec3 = vec3![0.0, 0.0, 1.0];

/// A type alias for a point in 3D space.
pub type Point = Vec3;
