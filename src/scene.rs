use crate::math::{self, Intersectable};
use crate::shapes::{colors, Color, Sphere};
use image::{DynamicImage, GenericImage, Rgba};

/// Describes the scene to be rendered.
#[derive(Debug)]
pub struct World {
    // TODO: Add support for different types of shapes
    pub objects: Vec<Sphere>,
    pub camera: Camera,
    pub background_color: Color,

    /// NOTE: Keep height and width private as we also need to update the aspect_ratio whenever
    ///     the height or width changes. Use the dedicated getters and setters.
    width: u32,
    height: u32,
    aspect_ratio: f64,
}

impl World {
    /// Creates a new `World`.
    ///
    /// Default constructor is disallowed due to private fields.
    /// A default camera set at (0, 0, 0) is also created.
    #[inline]
    pub fn new(width: u32, height: u32) -> World {
        World {
            width,
            height,
            background_color: colors::CYAN,
            objects: vec![],
            camera: Camera::at_origin(),
            aspect_ratio: width as f64 / height as f64,
        }
    }

    /// Pushes a new object to the scene.
    ///
    /// # Examples
    ///
    /// ```
    /// # use raytracer::{scene, shapes, math};
    /// let mut scene = scene::World::new(1920, 1080);
    /// let sphere = shapes::Sphere {
    ///     center: math::Vec3::new(0.0, 0.0, -5.0),
    ///     radius: 2.0,
    ///     color: shapes::colors::WHITE,
    /// };
    /// scene.add_object(sphere);
    ///
    /// assert_eq!(scene.objects.iter().count(), 1);
    /// ```
    #[inline]
    pub fn add_object(&mut self, object: Sphere) {
        self.objects.push(object)
    }

    /// Returns the current height of the scene.
    #[inline]
    pub fn get_height(&self) -> u32 {
        self.height
    }

    /// Sets the height to the given value.
    ///
    /// This also recalculates the aspect ratio.
    #[inline]
    pub fn set_height(&mut self, height: u32) {
        self.height = height;
        self.aspect_ratio = self.width as f64 / self.height as f64;
    }

    /// Returns the current width of the scene.
    #[inline]
    pub fn get_width(&self) -> u32 {
        self.width
    }

    /// Sets the width to the given value.
    ///
    /// This also recalculates the aspect ratio.
    #[inline]
    pub fn set_width(&mut self, width: u32) {
        self.width = width;
        self.aspect_ratio = self.width as f64 / self.height as f64;
    }

    /// Returns the current aspect ratio based on current height and width.
    ///
    /// The aspect ratio is updated whenever the height or width changes.
    ///
    /// # Examples
    ///
    /// ```
    /// # use raytracer::scene::World;
    /// let mut scene = World::new(10, 20);
    ///
    /// assert_eq!(scene.get_aspect_ratio(), 0.5);
    ///
    /// scene.set_width(20);
    /// assert_eq!(scene.get_aspect_ratio(), 1.0);
    /// ```
    #[inline]
    pub fn get_aspect_ratio(&self) -> f64 {
        self.aspect_ratio
    }

    /// Returns a `Ray` from the `Camera` to `(x, y)`.
    ///
    /// This method always creates a ray that goes *away* from the camera.
    /// It also performs some adjustments based on the scene's width, height and
    /// aspect ratio as well as the camera's FOV.
    #[inline]
    pub fn cast_prime(&self, x: f64, y: f64) -> math::Ray {
        let aspect_ratio = self.aspect_ratio;
        let width = self.width as f64;
        let height = self.height as f64;
        let fov = (self.camera.fov.to_radians() / 2.0).tan();

        let direction = math::Vec3 {
            x: ((((x + 0.5) / width) * 2.0 - 1.0) * aspect_ratio) * fov,
            y: (1.0 - ((y + 0.5) / height) * 2.0) * fov,
            z: -1.0,
        }
        .normalize();

        self.camera.cast_ray(direction)
    }

    /// Renders the scene into a `DynamicImage` object.
    ///
    // TODO: Support multiple objects.
    // BUG: Objects that should be behind other objects may appear in front instead
    #[inline]
    pub fn render(&self) -> DynamicImage {
        let mut image = DynamicImage::new_rgb8(self.width, self.height);

        for y in 0..self.height {
            for x in 0..self.width {
                let ray = self.cast_prime(x as f64, y as f64);

                let object = &self.objects[0];

                match object.intersect(&ray, 0.0, f64::MAX) {
                    Some(intersection) => {
                        // let gradient = 0.5 * ((1.0 - t) * unit_vector + (t * object.color));
                        let color = {
                            let unit_vector =
                                (intersection.point - math::Vec3::new(0.0, 0.0, -1.0)).unit();
                            0.5 * Color::new(
                                unit_vector.x + 1.0,
                                unit_vector.y + 1.0,
                                unit_vector.z + 1.0,
                            )
                        };
                        image.put_pixel(x, y, Rgba::from(color));
                    }
                    None => {
                        let gradient = {
                            let unit_vector = ray.direction.unit();
                            let t = 0.5 * (unit_vector.y + 1.0);
                            (1.0 - t) * colors::WHITE + (t * self.background_color)
                        };
                        image.put_pixel(x, y, Rgba::from(gradient));
                    }
                };
            }
        }

        image
    }
}

/// Simple camera with a customizable position in the scene.
#[derive(Debug)]
pub struct Camera {
    pub position: math::Vec3,
    pub fov: f64,
}

impl Camera {
    /// Returns a camera which position is at the origin with a default fov.
    #[inline]
    pub fn at_origin() -> Camera {
        Camera {
            position: math::Vec3::zero(),
            fov: 90.0,
        }
    }

    /// Returns a [`Ray`] pointing to `direction`.
    ///
    /// The origin is always the same as the camera.
    ///
    /// [`Ray`]: ../math/struct.Ray.html
    #[inline]
    pub fn cast_ray(&self, direction: math::Vec3) -> math::Ray {
        math::Ray {
            origin: self.position,
            direction,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{math, scene, shapes};
    use image::GenericImageView;

    #[inline]
    fn rand_world() -> scene::World {
        scene::World::new(10, 10)
    }

    fn rand_sphere() -> shapes::Sphere {
        shapes::Sphere {
            center: math::Vec3::new(0.0, 0.0, -5.0),
            radius: 2.0,
            color: shapes::colors::WHITE,
        }
    }

    #[test]
    fn scene_can_render() {
        let mut scene = rand_world();
        let sphere = rand_sphere();
        scene.add_object(sphere);
        let image = scene.render();

        assert_eq!(scene.get_width(), image.width());
        assert_eq!(scene.get_height(), image.height());
    }

    #[test]
    fn scene_add_object() {
        let mut scene = rand_world();
        let sphere = rand_sphere();
        scene.add_object(sphere);

        assert_eq!(scene.objects.iter().count(), 1);
    }

    #[test]
    fn scene_set_height() {
        let mut scene = rand_world();
        scene.set_height(20);

        assert_eq!(scene.get_height(), 20);
        assert_relative_eq!(scene.get_aspect_ratio(), 0.5);
    }

    #[test]
    fn scene_set_width() {
        let mut scene = rand_world();
        scene.set_width(20);

        assert_eq!(scene.get_width(), 20);
        assert_relative_eq!(scene.get_aspect_ratio(), 2.0);
    }

    #[test]
    fn camera_at_origin() {
        let camera = scene::Camera::at_origin();

        assert_eq!(camera.position, math::Vec3::zero());
        assert_relative_eq!(camera.fov, 90.0);
    }

    #[test]
    fn camera_cast_ray() {
        let camera = scene::Camera::at_origin();
        let direction = math::Vec3 {
            x: 0.0,
            y: 0.0,
            z: -1.0,
        };
        let ray = camera.cast_ray(direction);

        assert_eq!(ray.origin, camera.position);
        assert_eq!(ray.direction, direction);
    }
}
