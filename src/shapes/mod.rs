pub mod colors;

pub use colors::Color;

use crate::math;

/// Basic sphere for testing
#[derive(Debug)]
pub struct Sphere {
    pub center: math::Vec3,
    pub radius: f64,
    pub color: Color,
}

impl math::Intersectable for Sphere {
    #[inline]
    fn intersect(&self, ray: &math::Ray, t_min: f64, t_max: f64) -> Option<math::Intersection> {
        let oc = ray.origin - self.center;
        let a = ray.direction.dot(ray.direction);
        let half_b = oc.dot(ray.direction);
        let c = oc.dot(oc) - self.radius.powf(2.0);
        let discriminant = (half_b.powf(2.0)) - (a * c);

        if discriminant < 0.0 {
            return None;
        }

        let squared_discriminant = discriminant.sqrt();

        // Trying first hit root
        let mut root = (-half_b - squared_discriminant) / a;

        if root < t_min || t_max < root {
            // Trying second hit root
            root = (-half_b + squared_discriminant) / a;

            if root < t_min || t_max < root {
                return None;
            }
        }

        let point = ray.at(root);
        let normal = (point - self.center) / self.radius;

        let (normal, front_face) = math::Intersection::face_normal(&ray, normal);

        Some(math::Intersection {
            t: root,
            point,
            normal,
            front_face,
        })
    }
}

impl Default for Sphere {
    fn default() -> Self {
        Self {
            center: math::Vec3 {
                x: 0.0,
                y: 0.0,
                z: -5.0,
            },
            radius: 1.0,
            color: colors::WHITE,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::math::Intersectable;

    #[test]
    fn test_sphere_can_intersect() {
        let origin = math::Point::new(0.0, 0.0, 0.0);
        let sphere = Sphere {
            center: math::Vec3 {
                x: 0.0,
                y: 0.0,
                z: -5.0,
            },
            radius: 1.0,
            color: colors::WHITE,
        };
        let t_min = 0.0;
        let t_max = f64::MAX;

        let no_hit = math::Ray {
            origin,
            direction: math::Vec3::new(1.0, 1.0, 5.0),
        };

        let hit = math::Ray {
            origin,
            direction: math::Vec3::new(0.0, 0.0, -1.0),
        };

        assert!(sphere.intersect(&no_hit, t_min, t_max).is_none());

        let intersection = sphere.intersect(&hit, t_min, t_max);
        assert!(intersection.is_some());

        let intersection = intersection.unwrap();
        assert_relative_eq!(intersection.t, 4.0);
        assert_eq!(intersection.point, math::Vec3::new(0.0, 0.0, -4.0));
        assert_eq!(intersection.normal, math::Z_AXIS);
    }
}
