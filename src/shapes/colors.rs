// use std::ops::{Add, Mul};

use crate::math;

macro_rules! define_primary_colors {
    (
        $($color:ident = ($r:expr, $g:expr, $b:expr),)+
    ) => {
        $(
            pub const $color: Color = Color { x: $r, y: $g, z: $b };
        )+
    };
}

/// Represent a color using `rgba` format.
// #[derive(Debug, Copy, Clone)]
// pub struct Color {
//     pub r: u8,
//     pub g: u8,
//     pub b: u8,
// }

// TODO: Rethink Vec3 to avoid copying type everywhere.
pub type Color = math::Vec3;

// impl From<math::Vec3> for Color {
//     fn from(vec: math::Vec3) -> Self {
//         Self {
//             r: (vec.x.abs() * 255f64) as u8,
//             g: (vec.y.abs() * 255f64) as u8,
//             b: (vec.z.abs() * 255f64) as u8,
//         }
//     }
// }

// impl Add<Color> for Color {
//     type Output = Color;

//     fn add(self, rhs: Color) -> Self::Output {
//         let r: usize = (self.r as usize + rhs.r as usize) / 2;
//         let g: usize = (self.g as usize + rhs.g as usize) / 2;
//         let b: usize = (self.b as usize + rhs.b as usize) / 2;
//         Self {
//             r: r as u8,
//             g: g as u8,
//             b: b as u8,
//         }
//     }
// }

// impl Mul<f64> for Color {
//     type Output = Color;

//     fn mul(self, rhs: f64) -> Self::Output {
//         Self {
//             r: (((self.r as f64).sqrt() * rhs) * 255f64) as u8,
//             g: (((self.g as f64).sqrt() * rhs) * 255f64) as u8,
//             b: (((self.b as f64).sqrt() * rhs) * 255f64) as u8,
//         }
//     }
// }

// impl Mul<Color> for f64 {
//     type Output = Color;

//     fn mul(self, rhs: Color) -> Self::Output {
//         rhs * self
//     }
// }

impl From<Color> for image::Rgba<u8> {
    #[inline]
    fn from(color: Color) -> Self {
        Self::from([
            (color.x * 255f64) as u8,
            (color.y * 255f64) as u8,
            (color.z * 255f64) as u8,
            100,
        ])
    }
}

define_primary_colors!(
    BLACK = (0.0, 0.0, 0.0),
    WHITE = (1.0, 1.0, 1.0),
    RED = (1.0, 0.0, 0.0),
    GREEN = (0.0, 1.0, 0.0),
    BLUE = (0.0, 0.0, 1.0),
    CYAN = (0.0, 1.0, 1.0),
    YELLOW = (1.0, 1.0, 0.0),
    PINK = (1.0, 0.0, 1.0),
);
